class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    def add_product(self, product):
         self.products.append(product)

    def get_total_price(self):
        total_price_products = sum(product.get_price() for product in self.products)
        return total_price_products

    def get_total_quantity_of_products(self):
        total_quantity_products = sum(product.quantity for product in self.products)
        return total_quantity_products

    def purchase(self):
        self.purchased = True


class Product:
    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_price(self):
        return self.unit_price * self.quantity


# testing of functions
test_product2 = Product('Shoes', 100.0)
test_order = Order('adrian@example.com')
test_order.add_product("dwa")
print(test_order.products)

